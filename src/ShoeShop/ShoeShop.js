import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { shoeList } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeList,
    detailShoe: shoeList[0],
    cart: [],
  };

  handleChangeDetail = (shoe) => {
    this.setState({
      detailShoe: shoe,
    });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    // th1 : nếu findIndex không tìm  thấy => chưa có sản phẩm trong giỏ hàng => sẽ tạo mới object có thêm key là soLuong : 1 => push vào cloneCart
    // th2 : findIndex tìm thấy => đã có sản phẩm trong giỏ hàng => update key soLuong++ => không push
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  handleDeleteShoe = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  handleChangeQuantity = (shoe, value) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    cloneCart[index].soLuong += value;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <CartShoe
              cartShoe={this.state.cart}
              handleDeleteShoe={this.handleDeleteShoe}
              handleChangeQuantity={this.handleChangeQuantity}
            />
          </div>
          <div className="col-4">
            <ListShoe
              shoeArr={this.state.shoeArr}
              handleChangeDetail={this.handleChangeDetail}
              handleAddToCart={this.handleAddToCart}
            />
          </div>
        </div>
        <div>
          <DetailShoe detailShoe={this.state.detailShoe} />
        </div>
      </div>
    );
  }
}
