import React, { Component } from "react";

export default class CartShoe extends Component {
  renderDetailShoe = () => {
    return this.props.cartShoe.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, -1);
              }}
              className="btn btn-warning"
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, 1);
              }}
              className="btn btn-danger"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteShoe(item);
              }}
              className="btn border-danger text-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderDetailShoe()}</tbody>
        </table>
      </div>
    );
  }
}
