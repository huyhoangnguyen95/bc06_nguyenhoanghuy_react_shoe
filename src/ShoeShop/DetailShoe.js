import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { image, name, description } = this.props.detailShoe;
    return (
      <div>
        <div className="card">
          <img
            style={{ width: 200, margin: "auto" }}
            className="card-img-top"
            src={image}
            alt="Card image cap"
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{description}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Item 1</li>
            <li className="list-group-item">Item 2</li>
            <li className="list-group-item">Item 3</li>
          </ul>
        </div>
      </div>
    );
  }
}
