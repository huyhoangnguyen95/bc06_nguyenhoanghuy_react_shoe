import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-6">
        <div className="card text-center">
          <img
            style={{ width: 100, margin: "auto" }}
            className="card-img-top"
            src={image}
            alt
          />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">${price}</p>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.shoe);
              }}
              className="btn btn-warning"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.shoe);
              }}
              className="btn btn-success ml-3"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
